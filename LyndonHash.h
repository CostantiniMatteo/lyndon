#ifndef LYNDON_LYNDONHASH_H
#define LYNDON_LYNDONHASH_H

#include <utility>
#include <vector>
#include <string>
#include <sstream>
#include <set>
#include <map>
#include <tuple>

#include "MurmurHash3.h"
#include "BooPHF.h"

class LyndonHash {
public:
    typedef int T;
    typedef std::string id_t;  // Type of identifier
    typedef std::tuple<std::vector<T>, id_t, T> NodeInfo; // vector<T> --> int?
    typedef std::vector<T> Node;
    typedef std::vector<T> Edge;

    std::set<Node> source_nodes;
    std::vector<Node> nodes; // TODO: Remove later

    std::vector<bool> visited;

    explicit LyndonHash(const std::string &path);

    std::set<NodeInfo> get_node(const Node &n);
    std::set<T> get_edge(const Edge &e);
    bool is_visited(const LyndonHash::Node &n);

    std::vector<T> visit_from(const Node &n);

    class node_hasher {
    public:
        uint64_t operator()(const std::vector<T> &key, uint64_t seed = 0) const {
            uint64_t hash[2];
            MurmurHash3_x64_128((const void *) &key[0], sizeof(T) * key.size(), seed, hash);
            return hash[0];
        }
    };

    class edge_hasher {
    public:
        uint64_t operator()(const std::vector<T> &key, uint64_t seed = 0) const {
            uint64_t hash[2];
            MurmurHash3_x64_128((const void *) &key[0], sizeof(T) * key.size(), seed, hash);
            return hash[0];
        }
    };

    typedef boomphf::mphf<Node, node_hasher> nodes_phf;
    typedef boomphf::mphf<Edge, edge_hasher> edges_phf;


private:
    std::vector<std::set<NodeInfo>> nodes_hashtable;
    std::vector<std::set<T>> edges_hashtable;
    nodes_phf *nodes_mph;  // Nodes hash function
    edges_phf *edges_mph;  // Edges hash function

    std::vector<std::string> split(const std::string &s, char delim = ' ');

    std::tuple<std::vector<T>, id_t, T> parse_line(const std::string &line, char skip_if = '*');

    void parse_file(const std::string &path,
                    std::map<LyndonHash::Node, std::set<LyndonHash::NodeInfo>> &nodes_map,
                    std::map<LyndonHash::Edge, std::set<LyndonHash::T>> &edges_map,
                    std::map<LyndonHash::Node, bool> &source_nodes_map);
};

#endif //LYNDON_LYNDONHASH_H
