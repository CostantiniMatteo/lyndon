#include <fstream>
#include <utility>
#include <vector>
#include <string>
#include <sstream>
#include <set>
#include <map>
#include <tuple>
#include "LyndonHash.h"


std::vector<std::string> LyndonHash::split(const std::string &s, char delim) {
    std::vector<std::string> result;
    std::stringstream ss(s);
    std::string item;

    while (std::getline(ss, item, delim)) {
        result.push_back(item);
    }

    return result;
}

// Returns a tuple containing a vector with n-2 integers, the id and the position.
// Returns a default tuple if the line contains the character 'skip_if' (default '*')
// e.g.: 3 3 2 3  $ 0 0 0 0  $ 1 0 ---> ({3, 3, 2, 3, 0, 0, 0, 0}, 1, 0)
std::tuple<std::vector<LyndonHash::T>, LyndonHash::id_t, LyndonHash::T>
LyndonHash::parse_line(const std::string &line, char skip_if) {
    std::vector<T> tokens;

    if (line.find(skip_if) != std::string::npos) {
        return std::make_tuple(tokens, "", 0);
    }

    auto splitted = split(line, ' ');
    for (auto it = splitted.begin(); it != splitted.end() - 2; it++) {
        if ((*it) != "$" && it->find_first_not_of(' ') != std::string::npos) {
            tokens.push_back(stoi((*it)));
        }
    }

    return std::make_tuple(tokens, splitted[splitted.size() - 2], stoi(splitted[splitted.size() - 1]));
}


void LyndonHash::parse_file(const std::string &path,
                            std::map<LyndonHash::Node, std::set<LyndonHash::NodeInfo>> &nodes_map,
                            std::map<LyndonHash::Edge, std::set<LyndonHash::T>> &edges_map,
                            std::map<LyndonHash::Node, bool> &source_nodes_map) {
    std::ifstream infile(path);
    std::string line;

    // For each k-finger store the last k-1 values
    std::set<LyndonHash::Edge> reverse_edges;

    while (std::getline(infile, line)) {
        std::istringstream iss(line);
        auto current = parse_line(line);

        // There was a '*' in the line
        if (std::get<0>(current).empty()) {
            continue;
        }

        auto tokens = std::get<0>(current);
        auto id = std::get<1>(current);
        auto pos = std::get<2>(current);

        // Update nodes
        // Last two values are id and pos, the remaining first half is
        // the factorization, the second half is the bit vector
        auto count = tokens.size() / 2;
        Node node(tokens.begin(), tokens.begin() + count);

        auto node_it = nodes_map.find(node);
        // Node is not in map, initialize an empty set
        if (node_it == nodes_map.end()) {
            nodes_map[node] = std::set<NodeInfo>();
            node_it = nodes_map.find(node);
        }

        // Add (bit_vector, id, pos) for the current node
        auto res = node_it->second.insert(
                std::make_tuple(std::vector<T>(tokens.begin() + count, tokens.end()), id, pos)
        );


        // Update edges
        Edge edge(tokens.begin(), tokens.begin() + count - 1);
        Edge edge2(tokens.begin() + 1, tokens.begin() + count);

        auto edge_it = edges_map.find(edge);
        // Edge is not in map, initialize an empty set
        if (edge_it == edges_map.end()) {
            edges_map[edge] = std::set<T>();
            edge_it = edges_map.find(edge);
        }
        edge_it->second.insert(tokens[count - 1]);

        edge_it = edges_map.find(edge2);
        if (edge_it == edges_map.end()) {
            edges_map[edge2] = std::set<T>();
        }

        reverse_edges.insert(edge2);

        // Update source
        // Given a new node k = (k1 k2 k3 k4), k is not a source node if there is at
        // least one existing node ending in (k1, k2, k3)
        auto source_it = source_nodes_map.find(node);
        if (source_it == source_nodes_map.end()) {
            source_nodes_map[node] = true;
        }
        if (reverse_edges.find(edge) != reverse_edges.end()) {
            source_nodes_map[node] = false;
        }

        // Given a new node k = (k1 k2 k3 k4) if there were source nodes starting with
        // (k2, k3, k4) delete them because they have an incoming edge from k.
        std::vector<T> tmp(node.begin() + 1, node.end());

        edge_it = edges_map.find(tmp);
        if (edge_it == edges_map.end()) {
            continue;
        }

        for (auto n : edge_it->second) {
            tmp.push_back(n);
            source_nodes_map[tmp] = false;
            tmp.pop_back();
        }
    }

}

LyndonHash::LyndonHash(const std::string &path) {
    // Parse entries
    std::map<Node, std::set<NodeInfo>> nodes_map;
    std::map<Edge, std::set<T>> edges_map;
    std::map<Node, bool> source_nodes_map;
    parse_file(path, nodes_map, edges_map, source_nodes_map);

    std::vector<Node> nodes;
    std::vector<Edge> edges;

    // Retrieve all the nodes and edges in maps to build each mph
    for (auto it = nodes_map.begin(); it != nodes_map.end(); it++) {
        nodes.push_back(it->first);
    }

    for (auto it = edges_map.begin(); it != edges_map.end(); it++) {
        edges.push_back(it->first);
    }


    // BoohPHF parameters
    auto gamma_factor = 1.0;
    auto n_thread = 1;

    // Build nodes MPH
    auto nodes_iterator = boomphf::range(nodes.begin(), nodes.end());
    this->nodes_mph = new nodes_phf(
            nodes.size(), nodes_iterator, n_thread, gamma_factor, false
    );

    // Build edges MPH
    auto edges_iterator = boomphf::range(edges.begin(), edges.end());
    this->edges_mph = new edges_phf(
            edges.size(), edges_iterator, n_thread, gamma_factor, false
    );


    // Build hash tables
    std::vector<std::set<NodeInfo>> nodes_hashtable(nodes.size());
    std::vector<std::set<T>> edges_hashtable(edges.size());
    std::vector<bool> visited(nodes.size());

    for (auto it = nodes_map.begin(); it != nodes_map.end(); it++) {
        nodes_hashtable[this->nodes_mph->lookup(std::get<0>(*it))] = std::get<1>(*it);
        visited[this->nodes_mph->lookup(std::get<0>(*it))] = false;
    }

    for (auto it = edges_map.begin(); it != edges_map.end(); it++) {
        edges_hashtable[this->edges_mph->lookup(it->first)] = it->second;
    }


    // Build source_nodes set
    std::set<Node> source_nodes;
    for (auto pair : source_nodes_map) {
        if (pair.second) {
            source_nodes.insert(pair.first);
        }
    }

    std::swap(this->nodes_hashtable, nodes_hashtable);
    std::swap(this->edges_hashtable, edges_hashtable);
    std::swap(this->visited, visited);
    std::swap(this->source_nodes, source_nodes);
    std::swap(this->nodes, nodes);
}

std::set<LyndonHash::NodeInfo> LyndonHash::get_node(const LyndonHash::Node &n) {
    auto hash = this->nodes_mph->lookup(n);
    if (hash == ULLONG_MAX) {
        return std::set<LyndonHash::NodeInfo>();
    }
    return this->nodes_hashtable[hash];
}

std::set<LyndonHash::T> LyndonHash::get_edge(const LyndonHash::Edge &e) {
    auto hash = this->edges_mph->lookup(e);
    if (hash == ULLONG_MAX) {
        return std::set<LyndonHash::T>();
    }
    return this->edges_hashtable[hash];
}

bool LyndonHash::is_visited(const LyndonHash::Node &n) {
    auto hash = this->nodes_mph->lookup(n);
    if (hash == ULLONG_MAX) {
        return true;
    }
    return this->visited[hash];
}

std::vector<LyndonHash::T> LyndonHash::visit_from(const LyndonHash::Node &n) {
    std::vector<T> path;

    if (this->nodes_mph->lookup(n) != ULLONG_MAX) {
        this->visited[this->nodes_mph->lookup(n)] = true;
    }

    std::vector<T> curr(n.begin() + 1, n.end()); T next = -1; size_t max = 0;
    do {
        next = -1; max = 0;
        auto e_s = this->get_edge(curr);

        std::vector<T> tmp(curr.begin(), curr.end());
        for (auto k : e_s) {
            tmp.push_back(k);

            if (! this->is_visited(tmp)) {
                auto c = this->get_node(tmp).size();
                if (c > max) {
                    max = c;
                    next = k;
                }
            }

            tmp.pop_back();
        }

        if (next != -1) {
            path.push_back(next);
            curr.push_back(next);
            this->visited[this->nodes_mph->lookup(curr)] = true;
            curr.erase(curr.begin());
        }

    } while (next != -1); // There's at least one outgoing edge

    return path;
}
