#include <fstream>
#include <sstream>
#include <vector>
#include <set>
#include <map>
#include <tuple>
#include <utility>

#include "MurmurHash3.h"
#include "BooPHF.h"
#include "LyndonHash.h"


std::vector<std::string> split(const std::string &s, char delim);

int main(int argc, char *argv[]) {
    auto lyndon = LyndonHash(argv[1]);


    // Uncommet this to input a node from stdin
    // std::cout << "Query:" << std::endl;
    // std::string query;
    // std::getline(std::cin, query);
    std::string query = "2 2 27 9 3";

    std::vector<int> tokens;
    for (auto s : split(query, ' ')) {
        tokens.push_back(stoi(s));
    }

    auto path = lyndon.visit_from(tokens);

    std::cout << "Path found:" << std::endl;
    for (auto i : path) {
        std::cout << i << " ";
    }
    std::cout << std::endl;

    std::cout << "Source Nodes:" << std::endl;
    for (auto i : lyndon.source_nodes) {
        std::cout << i[0] << " " << i[1] << " " << i[2] << " " << i[3] << std::endl;
    }

    return 0;
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> result;
    std::stringstream ss(s);
    std::string item;

    while (std::getline(ss, item, delim)) {
        result.push_back(item);
    }

    return result;
}
